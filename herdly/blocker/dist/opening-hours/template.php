<?php
if (!defined('ABSPATH')) {
    exit; // Empêche l'accès direct au fichier
}

// Récupérer les horaires d'ouverture pour le post en cours
$post_id = get_the_ID();
$opening_times = get_post_meta($post_id, '_opening_times', true);

// Liste des jours de la semaine
$days_of_week = array(
    'monday' => __('Monday', 'herdly'),
    'tuesday' => __('Tuesday', 'herdly'),
    'wednesday' => __('Wednesday', 'herdly'),
    'thursday' => __('Thursday', 'herdly'),
    'friday' => __('Friday', 'herdly'),
    'saturday' => __('Saturday', 'herdly'),
    'sunday' => __('Sunday', 'herdly')
);

?>

<div <?php echo get_block_wrapper_attributes(); ?>>
    <div class="opening-hours-week">
        <?php foreach ($days_of_week as $day_key => $day_name): ?>
            <div class="opening-hours-day">
                <strong><?php echo esc_html($day_name); ?>:</strong>
                <?php
                // Vérifier si des horaires existent pour ce jour
                if (!empty($opening_times[$day_key]) && is_array($opening_times[$day_key])) {
                    $day_times = $opening_times[$day_key];

                    // Parcourir les plages horaires du jour
                    $times = array();
                    foreach ($day_times as $time_group) {
                        $start = esc_html($time_group['start']);
                        $stop = esc_html($time_group['stop']);
                        $times[] = "{$start} > {$stop}";
                    }
                    echo implode(' - ', $times);
                } else {
                    // Aucun horaire pour ce jour
                    esc_html_e('Closed', 'herdly');
                }
                ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
