/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!************************************!*\
  !*** ./blocks/menu-slider/view.js ***!
  \************************************/
document.addEventListener('DOMContentLoaded', function () {
  const slider = document.querySelector('.wp-block-herdly-menu-slider');
  const sliderItems = slider.querySelectorAll('.menu-item'); // Sélectionne chaque page du slider

  const prevButton = document.querySelector('.prev-btn');
  const nextButton = document.querySelector('.next-btn');
  const menuItems = document.querySelectorAll('.menu-item');
  const pageLinks = document.querySelectorAll('.page-link');
  let currentIndex = 0;
  const totalItems = menuItems.length;

  // Masquer la navigation si il n'y a qu'un seul élément
  if (totalItems === 1) {
    prevButton.style.display = 'none';
    nextButton.style.display = 'none';
    pageLinks.forEach(link => link.style.display = 'none');
  }
  function adjustSliderHeight() {
    let maxHeight = 0;

    // Parcourir chaque élément du slider pour trouver la plus grande hauteur
    sliderItems.forEach(item => {
      const itemHeight = item.offsetHeight; // Récupère la hauteur de chaque élément
      if (itemHeight > maxHeight) {
        maxHeight = itemHeight; // Met à jour la hauteur maximale
      }
    });

    // Applique la hauteur maximale au slider
    slider.style.height = `${maxHeight}px`;
  }

  // Ajuste la taille du slider au chargement de la page
  adjustSliderHeight();

  // Réajuste la taille du slider lors du redimensionnement de la fenêtre
  window.addEventListener('resize', function () {
    adjustSliderHeight();
  });

  // Fonction pour mettre à jour la visibilité des pages avec animation
  function updateSliderPosition() {
    menuItems.forEach((item, index) => {
      item.style.transition = 'none'; // Désactiver la transition avant modification
      if (index === currentIndex) {
        item.classList.add('active'); // Afficher la page active
        if (totalItems !== 1) {
          item.style.transition = 'opacity 1s ease, transform 1s ease'; // Réappliquer la transition
        }
      } else {
        item.classList.remove('active'); // Masquer les autres pages
      }
    });

    // Mettre à jour la classe active sur les titres
    pageLinks.forEach((link, index) => {
      link.classList.remove('--active'); // Retirer la classe '--active' de tous les liens
      if (index === currentIndex) {
        link.classList.add('--active'); // Ajouter la classe '--active' au lien correspondant à la page active
      }
    });
  }

  // Afficher la première page au départ
  updateSliderPosition();
  if (prevButton && nextButton) {
    // Fonction pour gérer la navigation par flèche gauche
    prevButton.addEventListener('click', function () {
      if (currentIndex > 0) {
        currentIndex--;
      } else {
        currentIndex = totalItems - 1; // Retourner à la dernière page
      }

      updateSliderPosition();
    });

    // Fonction pour gérer la navigation par flèche droite
    nextButton.addEventListener('click', function () {
      if (currentIndex < totalItems - 1) {
        currentIndex++;
      } else {
        currentIndex = 0; // Retour à la première page
      }

      updateSliderPosition();
    });
  }

  // Fonction pour la navigation par lien de page (en cliquant sur les titres)
  pageLinks.forEach(link => {
    link.addEventListener('click', function () {
      // Retirer la classe '--active' de tous les liens
      pageLinks.forEach(item => item.classList.remove('--active'));

      // Ajouter la classe '--active' au lien cliqué
      link.classList.add('--active');

      // Mettre à jour l'index de la page active
      const index = parseInt(this.getAttribute('data-index'), 10);
      currentIndex = index; // Mettre à jour l'index de la page active
      updateSliderPosition(); // Mettre à jour la page affichée
    });
  });

  if (totalItems !== 1) {
    const autoScroll = slider.getAttribute('data-autoscroll') === 'true';
    let scrollTime = parseInt(slider.getAttribute('data-scrolltime'), 10) * 1000; // Correction ici
    if (autoScroll) {
      // Défilement automatique à l'intervalle spécifié
      setInterval(function () {
        if (currentIndex < totalItems - 1) {
          currentIndex++;
        } else {
          currentIndex = 0; // Retour à la première page
        }

        updateSliderPosition();
      }, scrollTime); // Vous pouvez ajuster l'intervalle selon vos besoins
    }
  }
});
/******/ })()
;
//# sourceMappingURL=view.js.map