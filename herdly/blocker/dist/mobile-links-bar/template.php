<?php
/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */

// Récupérer les options
$google_maps = esc_url(get_option('gi_google_maps'));
$phone_number = esc_html(get_option('gi_phone_number'));
$mail_adress = esc_html(get_option('gi_mail_address'));
$facebook = esc_url(get_option('gi_facebook'));
$twitter = esc_url(get_option('gi_twitter'));
$instagram = esc_url(get_option('gi_instagram'));
$tiktok = esc_url(get_option('gi_tiktok'));
$linkedin = esc_url(get_option('gi_linkedin'));

$display_on_mobile = esc_url(get_option('mlb_display_on_mobile'));
$icons_style =get_option('mlb_icons_style');
$icons_color = get_option('mlb_icons_color');
if(!is_admin()){
    $class = $display_on_mobile ? 'mobile-only' : '';
}

// Fonction pour obtenir le contenu SVG
function get_svg_icon($icon_name, $style_folder) {
    // Construire le chemin vers l'icône SVG
    $svg_path = get_site_url() . '/wp-content/plugins/herdly/blocker/assets/icons/' . $style_folder . '/' . $icon_name . '.svg';
    // Vérifiez si le fichier existe

        return $svg_path;

}

?>


<div <?php echo esc_attr(get_block_wrapper_attributes()); ?> >
    <style>
        .chunk .svg-object svg *, .classic .svg-object svg *, .drawing .svg-object svg *,   .pixels .svg-object svg *, .round .svg-object svg *, .funky .smiley *, .funky .points, .funky .at, .funky:hover .heart{
            fill: <?php echo esc_attr($icons_color);?> !important;
        }
        .outline .svg-object svg *, .line .svg-object svg *{
            stroke: <?php echo esc_attr($icons_color);?> !important;

        }
        .funky .pointer-color{
            fill: <?php echo esc_attr($icons_color);?> !important;
            stroke: <?php echo esc_attr($icons_color);?> !important;

        }
        .social-links-nav a, .cls-1, .cls-2{
            color:<?php echo esc_attr($icons_color);?> !important;
        }
        .svg-object {
            pointer-events: none; /* Ajoutez ceci uniquement si nécessaire pour éviter les interférences avec l'élément SVG */
        }
    </style>
    <?php if ( is_admin() === true){ ?>
        <div class="mobile-links-bar <?php echo esc_attr($class)?> back-office-block">
    <?php } else { ?>
            <div class="mobile-links-bar <?php echo esc_attr($class)?> front-end-block">
    <?php } ?>

                <?php if (!$phone_number && !$google_maps && !$mail_adress){
                    if(!$facebook || !$twitter || !$instagram || !$linkedin){ ?>
                        <p class="mbl-error-message">*Please fill out the information in Herdly > General Info.</p>
                    <?php }
                }?>
        <?php if ($phone_number){ ?>
		<div class="phone-links <?php if($icons_style){ echo esc_attr($icons_style); } else { echo 'classic'; } ?>">
            <a href="tel:<?php echo esc_attr($phone_number)?>" title="Tel">
                <?php if($icons_color) {?>

                <?php } ?>
                <object
                        data="<?php if($icons_style){ echo get_svg_icon(esc_html('phone'), esc_attr($icons_style));} else {echo get_svg_icon(esc_html('phone'), esc_attr('classic')); }?>"
                        type="image/svg+xml"
                        class="svg-object">
                </object>

            </a>
        </div>
            <?php
        }
        if ($google_maps){
        ?>

        <div class="maps-links <?php if($icons_style){ echo esc_attr($icons_style); } else { echo 'classic'; } ?>">
            <a href="<?php echo $google_maps?>" title="Maps">
                <object
                        data="<?php if($icons_style){ echo get_svg_icon(esc_html('map'), esc_attr($icons_style));} else {echo get_svg_icon(esc_html('map'), esc_attr('classic')); }?>"
                        type="image/svg+xml"
                        class="svg-object">
                </object>
            </a>
        </div>
            <?php
        }
        if ($mail_adress){
            ?>
            <div class="mail-links <?php if($icons_style){ echo esc_attr($icons_style); } else { echo 'classic'; } ?>">
                <a href="mailto:<?php echo $mail_adress?>" title="Mail">
                    <object
                            data="<?php if($icons_style){ echo get_svg_icon(esc_html('mail'), esc_attr($icons_style));} else {echo get_svg_icon(esc_html('mail'), esc_attr('classic')); }?>"
                            type="image/svg+xml"
                            class="svg-object">
                    </object>
                </a>
            </div>
            <?php
        }
        if ($facebook || $twitter || $instagram || $linkedin){
            ?>
            <div class="social-links <?php if($icons_style){ echo esc_attr($icons_style); } else { echo 'classic'; } ?>">
                <div class="social-links-btn">
                    <object
                            data="<?php if($icons_style){ echo get_svg_icon(esc_html('social'), esc_attr($icons_style));} else {echo get_svg_icon(esc_html('social'), esc_attr('classic')); }?>"
                            type="image/svg+xml"
                            class="svg-object">
                    </object>

                </div>
            </div>
            <?php
            function extract_classes_from_block_wrapper_attributes($attributes) {
                // Extraire les classes de la chaîne d'attributs
                preg_match('/class="(.*?)"/', $attributes, $matches);
                $classes = isset($matches[1]) ? $matches[1] : '';

                // Séparer les classes en un tableau
                $classes_array = explode(' ', $classes);

                // Supprimer la classe "wp-block-blocker-mobile-links-bar" si elle est présente
                $classes_array = array_filter($classes_array, function($class) {
                    return $class !== 'wp-block-blocker-mobile-links-bar';
                });

                // Reconstruire la chaîne des classes
                $classes = implode(' ', $classes_array);

                return $classes;
            }


            ?>

            <div class="social-links-nav-wrapper">
                <div class="social-links-nav <?php echo esc_attr(extract_classes_from_block_wrapper_attributes(get_block_wrapper_attributes())); ?>" >
                    <?php if ($facebook) { ?>
                        <a href="<?php echo esc_url($facebook); ?>" title="Facebook"><span class="dashicons dashicons-facebook"></span> Facebook</a>
                    <?php } ?>
                    <?php if ($twitter) { ?>
                        <a href="<?php echo esc_url($twitter); ?>" title="Twitter"><span class="dashicons dashicons-twitter"></span> Twitter</a>
                    <?php } ?>
                    <?php if ($instagram) { ?>
                        <a href="<?php echo esc_url($instagram); ?>" title="Instagram"><span class="dashicons dashicons-instagram"></span> Instagram</a>
                    <?php } ?>
                    <?php if ($linkedin) { ?>
                        <a href="<?php echo esc_url($linkedin); ?>" title="Linkedin"><span class="dashicons dashicons-linkedin"></span> Linkedin</a>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>

    </div>

	</div>
</div>
