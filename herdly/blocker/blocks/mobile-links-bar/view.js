document.addEventListener('DOMContentLoaded', function() {
    // Code à exécuter une fois que le DOM est chargé
    const socialLinksNavWrapper = document.querySelector('.social-links-nav-wrapper');
    const mobileLinksBarWrapper = document.querySelector('.mobile-links-bar');

    console.log(socialLinksNavWrapper);
    console.log(mobileLinksBarWrapper);

    // Fonction pour mettre à jour la hauteur de socialLinksNavWrapper
    function updateSocialLinksNavHeight() {
        // Récupérer la hauteur de mobileLinksBarWrapper
        let mobileLinksBarWrapperHeight = mobileLinksBarWrapper.clientHeight;
        console.log(mobileLinksBarWrapperHeight);
        socialLinksNavWrapper.style.bottom = `${mobileLinksBarWrapperHeight}px`;
    }

    // Mettre à jour la hauteur initiale
    updateSocialLinksNavHeight();

    // Écouter l'événement resize de la fenêtre
    window.addEventListener('resize', updateSocialLinksNavHeight);

    // Ajouter un événement click au bouton socialLinksBtn s'il existe
    let socialLinksBtn = document.querySelector('.social-links-btn');
    if (socialLinksBtn) {
        let socialLinkNav = document.querySelector('.social-links-nav');
        socialLinksBtn.addEventListener('click', function() {
            socialLinkNav.classList.toggle('--active');
        });
    }

        fetch('/wp-json/herdly/v1/mlb_icons_color')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Erreur lors de la récupération de la couleur des icônes');
                }
                return response.json();
            })
            .then(color => {
                console.log('Couleur récupérée :', color);

                if (!color) {
                    console.warn('Aucune couleur trouvée, le stroke ne sera pas modifié.');
                    return;
                }
                const svgObjects = document.querySelectorAll('.mobile-links-bar object');
                svgObjects.forEach(function(svgObject) {
                    const svgDoc = svgObject.contentDocument;
                    const paths = svgDoc.querySelectorAll('path, polygon');
                    paths.forEach(path => {
                        path.style.setProperty('stroke', color, 'important');
                        path.style.setProperty('fill', color, 'important');
                    });

                });
            })
            .catch(error => {
                console.error('Erreur lors de la récupération ou de l\'application de la couleur :', error);
            });





});