import { __ } from '@wordpress/i18n';
import { useBlockProps } from '@wordpress/block-editor';
import { useEffect, useState } from '@wordpress/element';
import './editor.scss';

export default function Edit() {
	// Structure par défaut des horaires si aucun n'est défini
	const defaultOpeningHours = {
		monday: __('Closed', 'herdly'),
		tuesday: __('Closed', 'herdly'),
		wednesday: __('Closed', 'herdly'),
		thursday: __('Closed', 'herdly'),
		friday: __('Closed', 'herdly'),
		saturday: __('Closed', 'herdly'),
		sunday: __('Closed', 'herdly'),
	};

	// Utilisation du state pour gérer les horaires
	const [openingHours, setOpeningHours] = useState(defaultOpeningHours);

	// Récupération des données depuis l'API REST
	useEffect(() => {
		// Obtenez l'ID du post actuel dans l'éditeur
		const postId = wp.data.select('core/editor').getCurrentPostId();

		if (postId) {
			// Récupérer le type de post pour vérifier qu'il s'agit d'un "location"
			const postType = wp.data.select('core/editor').getCurrentPostType();

			if (postType === 'location') {  // Vérifiez si le type de post est bien "location"
				wp.apiRequest({

					path: `/herdly/v1/location/${postId}`, // Récupérer les données du CPT "location"
					method: 'GET',
				}).then((data) => {
					if (data) {
						// Mettre à jour l'état avec les horaires récupérés
						setOpeningHours(data);
					} else {
						setOpeningHours(defaultOpeningHours);
					}
				}).catch((error) => {
					console.error('Erreur lors de la récupération des horaires :', error);
				});
			}
		}
	}, []);

	return (
		<div {...useBlockProps()} className="opening-hours-list">
				{Object.keys(openingHours).map((day) => (
					<div key={day} className="opening-hours-day">
						<strong>{__(day.charAt(0).toUpperCase() + day.slice(1), 'herdly')}:</strong>{' '}
						<div>
						{Array.isArray(openingHours[day]) && openingHours[day].length > 0 ? (
							openingHours[day].map((time, index) => (
								<span key={index}>
                                    {time.start} > {time.stop}
									{index < openingHours[day].length - 1 && ' - '}
                                </span>
							))
						) : (
							<span>{__('Closed', 'herdly')}</span>
						)}
						</div>
					</div>
				))}
			</div>
	);
}
