/**
 * Registers a new block provided a unique name and an object defining its behavior.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/
 */
import { registerBlockType } from '@wordpress/blocks';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * All files containing `style` keyword are bundled together. The code used
 * gets applied both to the front of your site and to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './style.scss';
import './editor.scss';

/**
 * Internal dependencies
 */
import Edit from './edit';
import metadata from './block.json';

/**
 * Registers the block with the metadata from block.json.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-metadata/
 */
registerBlockType(metadata.name, {
	/**
	 * Renders the block editor interface for the block.
	 *
	 * @see ./edit.js
	 */
	edit: Edit,

	/**
	 * Since this is a dynamic block, we don't save anything to the database.
	 * The content is rendered on the server-side using PHP.
	 *
	 * @see ./template.php
	 */
	save: () => null,
});
