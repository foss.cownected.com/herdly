import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, ToggleControl, RangeControl } from '@wordpress/components';

const Edit = ({ attributes, setAttributes }) => {
    const { autoScroll, showPageNavigation, scrollTime, showPrevNextButton, fixedPrevNextButton } = attributes;

    // Valeur par défaut si non définie
    const defaultScrollTime = scrollTime || 3;

    const blockProps = useBlockProps();

    return (
        <div {...blockProps}>
            <InspectorControls>
                <PanelBody title="Slider Settings" initialOpen={true}>
                    <ToggleControl
                        label="Show Page Navigation"
                        checked={showPageNavigation}
                        onChange={(value) => setAttributes({ showPageNavigation: value })}
                    />
                    <ToggleControl
                        label="Show Previous and Next Button"
                        checked={showPrevNextButton}
                        onChange={(value) => setAttributes({ showPrevNextButton: value })}
                    />
                    {showPrevNextButton && (
                        <ToggleControl
                            label="Fixed Previous and Next Button"
                            checked={fixedPrevNextButton}
                            onChange={(value) => setAttributes({ fixedPrevNextButton: value })}
                        />
                    )}

                    <br/>
                    <ToggleControl
                        label="Auto Scroll"
                        checked={autoScroll}
                        onChange={(value) => setAttributes({ autoScroll: value })}
                    />

                    {/* Affiche le RangeControl uniquement si autoScroll est activé */}
                    {autoScroll && (
                        <RangeControl
                            label="Scroll Time (en s)"
                            value={defaultScrollTime}
                            onChange={(value) => setAttributes({ scrollTime: value })}
                            min={1}
                            max={10}
                            step={1}
                        />
                    )}
                </PanelBody>
            </InspectorControls>

            <div className="menu-slider-preview">
                {showPageNavigation && (
                    <div className="page-navigation">
                        <p>Page 1</p>
                        <p>Page 2</p>
                        <p>Page 3</p>
                        <p>...</p>
                    </div>
                )}
                <div className='slider-info'>
                    <p className="title">Preview of the Menu Slider</p>
                    {autoScroll ? 'Automatic Scrolling On' : 'Automatic Scrolling Off'}
                </div>
                {showPrevNextButton && (
                <div className="slider-controls">
                    <button className="prev-btn">&#8592;</button> {/* Flèche gauche */}
                    <button className="next-btn">&#8594;</button> {/* Flèche droite */}
                </div>
                )}

            </div>
        </div>
    );
};

export default Edit;
