<?php
$show_page_navigation = isset($attributes['showPageNavigation']) ? $attributes['showPageNavigation'] : false;
$auto_scroll = isset($attributes['autoScroll']) ? $attributes['autoScroll'] : false;
$scroll_time = isset($attributes['scrollTime']) ? $attributes['scrollTime'] : 5;
$show_buttons = isset($attributes['showPrevNextButton']) ? $attributes['showPrevNextButton'] : false;
$fixed_buttons = isset($attributes['fixedPrevNextButton']) ? $attributes['fixedPrevNextButton'] : false;
$isMenuShared = get_option('herdly_menu_shared');

global $wpdb;
$current_post_id = get_the_ID();

// Récupérer uniquement les postmeta où meta_key = _associated_locations
$results = $wpdb->get_results($wpdb->prepare(
    "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = %s",
    '_associated_locations'
));

$associated_pages = [];

if ($results) {
    foreach ($results as $row) {
        $locations = maybe_unserialize($row->meta_value);

        // Vérifier si c'est un tableau et si l'ID du post en cours y est inclus
        if (is_array($locations) && in_array($current_post_id, $locations)) {
            $associated_pages[] = $row->post_id;
        }
    }
}


$args = [];

if ($isMenuShared) {
    $args = array(
        'post_type' => 'menupage',
        'posts_per_page' => -1,
    );
} if (empty($associated_pages)) {
    if (current_user_can('administrator')) {
        // Si l'utilisateur a le rôle d'administrateur
        echo '<div class="empty-menu admin-message">There is no Menu Page associated with this restaurant. Please check your menu pages, or delete the block "Menu Slider" on this page</div>';
    } else {
        // Si l'utilisateur n'a pas le rôle d'administrateur
        echo '<div class="empty-menu">Sorry, There is no Menu Page associated with this restaurant. Please contact us...</div>';
    }
} else {
    // Afficher uniquement les menus associés
    $args = array(
        'post_type' => 'menupage',
        'posts_per_page' => -1,
        'post__in' => $associated_pages,
    );
}

$menu_pages = new WP_Query($args);

if ($menu_pages->have_posts()) : ?>
    <div class="menu-slider-wrapper">
        <?php if ($show_page_navigation): ?>
            <div class="page-navigation">
                <?php
                $index = 0;
                while ($menu_pages->have_posts()) : $menu_pages->the_post();
                    ?>
                    <p class="page-link" data-index="<?php echo $index; ?>"><?php the_title(); ?></p>
                    <?php
                    $index++;
                endwhile;
                ?>
            </div>
        <?php endif; ?>

        <div class="wp-block-herdly-menu-slider" data-autoscroll="<?php echo esc_attr($auto_scroll ? 'true' : 'false'); ?>" data-scrolltime="<?php echo esc_attr($scroll_time); ?>" data-prevnextbuttons="<?php echo esc_attr($show_buttons ? 'true' : 'false'); ?>"  data-fixedbuttons="<?php echo esc_attr($fixed_buttons ? 'true' : 'false'); ?>">
            <?php while ($menu_pages->have_posts()) : $menu_pages->the_post(); ?>
                <div class="menu-item">
                    <div><?php the_content(); ?></div>
                </div>
            <?php endwhile; ?>
        </div>

        <?php if ($show_buttons): ?>
            <?php if ($fixed_buttons): ?>
                <style>
                    .prev-next-btn {
                        position: fixed !important;
                    }
                </style>
            <?php endif; ?>
            <div class="slider-controls">
                <div class="prev-btn prev-next-btn">
                    <div class="arrow">&#8592;</div>
                </div>
                <div class="next-btn prev-next-btn">
                    <div class="arrow">&#8594;</div>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <?php wp_reset_postdata();
endif;
?>
