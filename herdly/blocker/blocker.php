<?php
/**
 * Plugin Name:       Herdly Blocks
 * Description:       Just dynamic blocks...
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       herdly
 *
 * @package           herdly
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */

//OTHER FUNCTION
define('BLOCKER_URL', plugin_dir_url(__FILE__));



function blocker_init() {
    register_block_type(__DIR__ . '/dist/mobile-links-bar', array('render_callback' => 'herdly_mobile_links_bar_render_callback'));
    register_block_type(__DIR__ . '/dist/opening-hours', array('render_callback' => 'herdly_opening_hours_render_callback'));

}
add_action( 'init', 'blocker_init' );


function menu_slider_init(){
    register_block_type(__DIR__ . '/dist/menu-slider', array('render_callback' => 'herdly_menu_slider_render_callback'));

}
$isRestaurant = get_option('herdly_is_restaurant');

if ($isRestaurant){
    add_action( 'init', 'menu_slider_init' );
};

// LINKS BAR
function herdly_mobile_links_bar_render_callback( $atts, $content, $block){
    // Naming convention: '{namespace}-{blockname}-view-script'
    wp_enqueue_script( 'herdly-mobile-links-bar-view-script');
    ob_start();
    require plugin_dir_path(__FILE__) . 'dist/mobile-links-bar/template.php';
    return ob_get_clean();
}

// OPENING HOURS
function herdly_opening_hours_render_callback( $atts, $content, $block){
    // Naming convention: '{namespace}-{blockname}-view-script'
    wp_enqueue_script( 'herdly-opening-hours-view-script');
    ob_start();
    require plugin_dir_path(__FILE__) . 'dist/opening-hours/template.php';
    return ob_get_clean();
}

// MENU SLIDER
function herdly_menu_slider_render_callback( $atts, $content, $block ) {
    // Charger le script côté front-end
    wp_enqueue_script( 'herdly-menu-slider-view-script' );

    // Rendre les attributs disponibles pour le template
    $attributes = $atts;

    // Capture la sortie du template
    ob_start();
    require plugin_dir_path(__FILE__) . 'dist/menu-slider/template.php';
    return ob_get_clean();
}


//DASHICON
function load_dashicons() {
    wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'load_dashicons');
add_action('admin_enqueue_scripts', 'load_dashicons');




