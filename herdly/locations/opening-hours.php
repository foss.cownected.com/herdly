<?php

if (!defined('ABSPATH')) {
    exit; // Empêche l'accès direct au fichier
}

// Ajouter un méta-box pour les horaires d'ouverture dans l'éditeur de publications
function add_opening_time_metabox() {
    add_meta_box(
        'opening_time_metabox',
        __('Opening Time', 'herdly'),
        'display_opening_time_metabox',
        'location',
        'side',
        'high'
    );
}
add_action('add_meta_boxes', 'add_opening_time_metabox');

// Afficher le méta-box pour les horaires d'ouverture
function display_opening_time_metabox($post) {
    // Générer un nonce pour la sécurité
    wp_nonce_field('save_opening_times', 'opening_time_nonce');

    // Récupérer les horaires d'ouverture enregistrés
    $opening_times = get_post_meta($post->ID, '_opening_times', true);
    if (empty($opening_times)) {
        $opening_times = array(); // Initialiser un tableau vide si aucun horaire n'est enregistré
    }

    // Liste des jours de la semaine
    $days_of_week = array(
        'monday' => __('Monday', 'herdly'),
        'tuesday' => __('Tuesday', 'herdly'),
        'wednesday' => __('Wednesday', 'herdly'),
        'thursday' => __('Thursday', 'herdly'),
        'friday' => __('Friday', 'herdly'),
        'saturday' => __('Saturday', 'herdly'),
        'sunday' => __('Sunday', 'herdly')
    );

    // Parcourir chaque jour de la semaine
    foreach ($days_of_week as $day_key => $day_name) {
        echo '<div class="day-section" data-day="' . esc_attr($day_key) . '">';
        echo '<h4>' . esc_html($day_name) . '</h4>';

        // Récupérer les horaires d'ouverture pour ce jour
        $day_opening_times = isset($opening_times[$day_key]) ? $opening_times[$day_key] : array();

        // Afficher les champs "start" et "stop" pour chaque groupe d'horaires
        foreach ($day_opening_times as $index => $time_group) {
            echo '<div class="time-group" data-index="' . esc_attr($index) . '">';
            echo '<input type="time" name="opening_time_' . esc_attr($day_key) . '_start[]" value="' . esc_attr($time_group['start']) . '" required />';
            echo ' > ';
            echo '<input type="time" name="opening_time_' . esc_attr($day_key) . '_stop[]" value="' . esc_attr($time_group['stop']) . '" required />';
            echo '<button type="button" class="remove-time-group button">✕</button>';
            echo '</div>';
        }

        // Ajouter un bouton pour ajouter un nouveau groupe de temps pour le jour
        echo '<button type="button" class="add-time-group button button-small" data-day="' . esc_attr($day_key) . '">add a time slot</button>';
        echo '</div>';
    }
}


// Enregistrer les horaires d'ouverture lors de la sauvegarde d'un post
function save_opening_time_metabox($post_id) {
    // Vérifier les autorisations et les sauvegardes automatiques
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Vérifier le nonce pour la sécurité
    if (!isset($_POST['opening_time_nonce']) || !check_admin_referer('save_opening_times', 'opening_time_nonce')) {
        return;
    }

    // Liste des jours de la semaine
    $days_of_week = array(
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday'
    );

    // Tableau pour stocker les horaires d'ouverture par jour
    $opening_times = array();

    // Traiter chaque jour de la semaine
    foreach ($days_of_week as $day) {
        // Vérifier la présence des entrées pour le jour spécifique
        if (isset($_POST["opening_time_{$day}_start"]) && isset($_POST["opening_time_{$day}_stop"])) {
            $day_opening_times = array();

            // Boucle à travers chaque groupe d'horaires
            foreach ($_POST["opening_time_{$day}_start"] as $index => $start_time) {
                $stop_time = $_POST["opening_time_{$day}_stop"][$index];

                // Nettoyer les entrées et s'assurer qu'elles sont valides
                $start_time_clean = sanitize_text_field($start_time);
                $stop_time_clean = sanitize_text_field($stop_time);

                // Ne traiter que les entrées valides
                if (!empty($start_time_clean) && !empty($stop_time_clean)) {
                    $day_opening_times[] = array(
                        'start' => $start_time_clean,
                        'stop' => $stop_time_clean,
                    );
                }
            }

            // Si des horaires ont été définis pour le jour, les ajouter au tableau des horaires d'ouverture
            if (!empty($day_opening_times)) {
                $opening_times[$day] = $day_opening_times;
            }
        }
    }

    // Enregistrer les méta-données pour les horaires d'ouverture
    update_post_meta($post_id, '_opening_times', $opening_times);
}
add_action('save_post', 'save_opening_time_metabox');

// Enqueue les scripts pour l'interface d'administration
function enqueue_opening_time_scripts($hook) {
    // Vérifier que nous sommes sur une page de publication ou d'édition
    if ($hook === 'post.php' || $hook === 'post-new.php') {
        // Chemin vers le fichier JavaScript
        $script_url = plugin_dir_url(dirname(__FILE__)) . 'admin/js/opening-time.js';

        // Enregistrer le script
        wp_enqueue_script(
            'opening-time-script', // Identifiant du script
            $script_url, // URL du script
            array('jquery'), // Dépendances
            '1.0', // Version du script
            true // Charger dans le pied de page
        );
    }
}
add_action('admin_enqueue_scripts', 'enqueue_opening_time_scripts');
