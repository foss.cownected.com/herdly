<?php

if (!defined('ABSPATH')) {
    die();
}

// Ajouter une métaboxe pour la localisation
function add_adress_metabox() {
    add_meta_box(
        'location_adress',
        __('Address', 'herdly'), // Titre de la métaboxe
        'display_adress_metabox',
        'location', // Custom post type (doit correspondre au nom du custom post type enregistré)
        'side', // Contexte (peut être 'normal', 'side', 'advanced')
        'high' // Priorité (peut être 'high', 'low')
    );
}
add_action('add_meta_boxes', 'add_adress_metabox');

// Fonction pour afficher la métaboxe de localisation
function display_adress_metabox($post) {
    // Récupérer les valeurs de la localisation enregistrée
    $number = get_post_meta($post->ID, '_location_number', true);
    $street = get_post_meta($post->ID, '_location_street', true);
    $postcode = get_post_meta($post->ID, '_location_postcode', true);
    $city = get_post_meta($post->ID, '_location_city', true);
    $country = get_post_meta($post->ID, '_location_country', true);

    // Créer le formulaire
    echo '<form id="location_metabox_form">';

    // Afficher les champs de texte pour les parties de l'adresse
    echo '<div class="form-row">';

    echo '<div class="form-element">';
    echo '<label for="location_number">' . __('Number', 'herdly') . '</label>';
    echo '<input type="number" id="location_number" name="location_number" value="' . esc_attr($number) . '" class="widefat" required />';
    echo '</div>';

    echo '<div class="form-element">';
    echo '<label for="location_street">' . __('Street', 'herdly') . '</label>';
    echo '<input type="text" id="location_street" name="location_street" value="' . esc_attr($street) . '" class="widefat" required />';
    echo '</div>';

    echo '</div>';
    echo '<div class="form-row">';

    echo '<div class="form-element">';
    echo '<label for="location_postcode">' . __('Post Code', 'herdly') . '</label>';
    echo '<input type="number" id="location_postcode" name="location_postcode" value="' . esc_attr($postcode) . '" class="widefat" required />';
    echo '</div>';

    echo '<div class="form-element">';
    echo '<label for="location_city">' . __('City', 'herdly') . '</label>';
    echo '<input type="text" id="location_city" name="location_city" value="' . esc_attr($city) . '" class="widefat" required />';
    echo '</div>';

    echo '</div>';

    echo '<div class="form-element">';
    echo '<label for="location_country">' . __('Country', 'herdly') . '</label>';
    echo '<input type="text" id="location_country" name="location_country" value="' . esc_attr($country) . '" class="widefat" />';
    echo '</div>';

    // Ajouter le bouton de réinitialisation
    echo '<input type="reset" value="Reset" class="button-secondary" />';

    // Fermer le formulaire
    echo '</form>';
}

// Enregistrer les données de la localisation lorsque le post est sauvegardé
function save_location_adress($post_id) {
    // Vérifier les permissions de l'utilisateur
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Enregistrer les champs de localisation si soumis
    if (isset($_POST['location_number'])) {
        $number = sanitize_text_field($_POST['location_number']);
        update_post_meta($post_id, '_location_number', $number);
    }

    if (isset($_POST['location_street'])) {
        $street = sanitize_text_field($_POST['location_street']);
        update_post_meta($post_id, '_location_street', $street);
    }

    if (isset($_POST['location_postcode'])) {
        $postcode = sanitize_text_field($_POST['location_postcode']);
        update_post_meta($post_id, '_location_postcode', $postcode);
    }

    if (isset($_POST['location_city'])) {
        $city = sanitize_text_field($_POST['location_city']);
        update_post_meta($post_id, '_location_city', $city);
    }

    if (isset($_POST['location_country'])) {
        $country = sanitize_text_field($_POST['location_country']);
        update_post_meta($post_id, '_location_country', $country);
    }
}
add_action('save_post_location', 'save_location_adress');
