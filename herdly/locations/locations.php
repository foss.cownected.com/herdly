<?php

if (!defined('ABSPATH'))
{
die();
}


// Register Custom Post Type Location
function create_location_cpt() {

$labels = array(
'name' => _x( 'Locations', 'Post Type General Name', 'herdly' ),
'singular_name' => _x( 'Location', 'Post Type Singular Name', 'herdly' ),
'menu_name' => _x( 'Locations', 'Admin Menu text', 'herdly' ),
'name_admin_bar' => _x( 'Location', 'Add New on Toolbar', 'herdly' ),
'archives' => __( 'Location Archives', 'herdly' ),
'attributes' => __( 'Location Attributes', 'herdly' ),
'parent_item_colon' => __( 'Parent Location:', 'herdly' ),
'all_items' => __( 'All Locations', 'herdly' ),
'add_new_item' => __( 'Add New Location', 'herdly' ),
'add_new' => __( 'Add New', 'herdly' ),
'new_item' => __( 'New Location', 'herdly' ),
'edit_item' => __( 'Edit Location', 'herdly' ),
'update_item' => __( 'Update Location', 'herdly' ),
'view_item' => __( 'View Location', 'herdly' ),
'view_items' => __( 'View Locations', 'herdly' ),
'search_items' => __( 'Search Location', 'herdly' ),
'not_found' => __( 'Not found', 'herdly' ),
'not_found_in_trash' => __( 'Not found in Trash', 'herdly' ),
'featured_image' => __( 'Featured Image', 'herdly' ),
'set_featured_image' => __( 'Set featured image', 'herdly' ),
'remove_featured_image' => __( 'Remove featured image', 'herdly' ),
'use_featured_image' => __( 'Use as featured image', 'herdly' ),
'insert_into_item' => __( 'Insert into Location', 'herdly' ),
'uploaded_to_this_item' => __( 'Uploaded to this Location', 'herdly' ),
'items_list' => __( 'Locations list', 'herdly' ),
'items_list_navigation' => __( 'Locations list navigation', 'herdly' ),
'filter_items_list' => __( 'Filter Locations list', 'herdly' ),
);
$args = array(
'label' => __( 'Location', 'herdly' ),
'description' => __( 'Different locations', 'herdly' ),
'labels' => $labels,
'menu_icon' => 'dashicons-admin-multisite',
'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
'taxonomies' => array(),
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'menu_position' => 5,
'show_in_admin_bar' => true,
'show_in_nav_menus' => true,
'can_export' => true,
'has_archive' => true,
'hierarchical' => false,
'exclude_from_search' => false,
'show_in_rest' => true,
'rewrite' => array('slug' => 'locations'),
'publicly_queryable' => true,
'capability_type' => 'post',
);
register_post_type( 'location', $args );

}
add_action( 'init', 'create_location_cpt', 0 );



require_once(plugin_dir_path(__FILE__) . 'adress.php');
require_once(plugin_dir_path(__FILE__) . 'opening-hours.php');

// Ajouter des colonnes personnalisées à la liste des "Locations"
function add_location_columns($columns) {
    // Insérer la colonne pour l'adresse après la colonne "Title"
    $columns['location_address'] = __('Address', 'herdly');
    return $columns;
}
add_filter('manage_location_posts_columns', 'add_location_columns');

// Remplir les colonnes personnalisées avec les données
function custom_location_column($column, $post_id) {
    switch ($column) {
        case 'location_address':
            // Récupérer les valeurs des métadonnées d'adresse
            $number = get_post_meta($post_id, '_location_number', true);
            $street = get_post_meta($post_id, '_location_street', true);
            $postcode = get_post_meta($post_id, '_location_postcode', true);
            $city = get_post_meta($post_id, '_location_city', true);
            $country = get_post_meta($post_id, '_location_country', true);

            // Vérifier si une adresse est disponible
            if ($number || $street || $postcode || $city || $country) {
                // Afficher l'adresse complète
                echo esc_html("$number $street, $postcode $city, $country");
            } else {
                // Aucun champ d'adresse rempli
                echo __('No address provided', 'herdly');
            }
            break;
    }
}
add_action('manage_location_posts_custom_column', 'custom_location_column', 10, 2);

// Ajouter une colonne pour la featured image
function add_featured_image_column($columns) {
    $columns['featured_image'] = __('Featured Image', 'herdly');
    return $columns;
}
add_filter('manage_location_posts_columns', 'add_featured_image_column');

// Afficher la featured image dans la nouvelle colonne
function display_featured_image_column($column, $post_id) {
    if ($column == 'featured_image') {
        $featured_image = get_the_post_thumbnail($post_id, array(50, 50)); // Taille de l'image (50x50px)
        if ($featured_image) {
            echo $featured_image;
        } else {
            echo __('No Image', 'herdly');
        }
    }
}
add_action('manage_location_posts_custom_column', 'display_featured_image_column', 10, 2);


function enqueue_location_styles($hook) {
    // Charger les styles uniquement dans l'interface d'administration
    if ($hook == 'post.php' || $hook == 'post-new.php') {
        // Charger la feuille de style pour la métaboxe d'adresse
        wp_enqueue_style('location-styles', plugin_dir_url(dirname(__FILE__)) . 'admin/css/css.css');
    }
}
add_action('admin_enqueue_scripts', 'enqueue_location_styles');

function enqueue_location_scripts($hook) {
    // Charger les scripts uniquement dans l'interface d'administration
    if ($hook == 'post.php' || $hook == 'post-new.php') {
        // Charger le fichier JavaScript pour la métaboxe
        wp_enqueue_script('location-scripts', plugin_dir_url(dirname(__FILE__)) . 'admin/js/locations.js', array(), '1.0', true);
    }
}

add_action('admin_enqueue_scripts', 'enqueue_location_scripts');



