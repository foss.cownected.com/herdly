== HERDLY ==
Contributors: cownected
Donate link: https://www.cownected.com/
Tags: blocks, entreprise, company, oppening, menu, restaurant
Requires at least: 6.3
Tested up to: 6.3
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Share your WordPress posts to Microsoft Teams with a single click!

== Description ==

**Features:**



== Installation ==



== Usage ==


== Frequently Asked Questions ==

=provide you with a URL.

== Screenshots ==
1. Screenshot of the plugin settings page
2. Screenshot of the editor sidebar with the megaphone button

== Changelog ==

= 1.0.0 =
* Initial release of MS Teams Publisher.

== Upgrade Notice ==

= 1.0.0 =
* Initial release of MS Teams Publisher.

== License ==
This plugin is licensed under the GPLv2 or later.
