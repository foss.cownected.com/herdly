<?php
/*
 * Plugin Name: Herdly
 * Plugin URI: https://www.cownected.com/herdly
 * Description: A cooking management plugin for locations.
 * Version: 1.0
 * Author: cownected
 * Author URI: https://www.cownected.com
 * License: GPL2
 * Text Domain: herdly
 * @package herdly
 */

require_once(plugin_dir_path(__FILE__) . 'pages/settings.php');
require_once(plugin_dir_path(__FILE__) . 'locations/locations.php');
require_once(plugin_dir_path(__FILE__) . 'the-menu/the-menu.php');
require_once(plugin_dir_path(__FILE__) . 'blocker/blocker.php');

// Inclure les fichiers d'API
require_once(plugin_dir_path(__FILE__) . 'api/api-routes.php');

// CHANGE PERMALINKS
function my_plugin_set_permalinks() {
    // Définir les permaliens sur "Post name"
    update_option('permalink_structure', '/%postname%/');

    // Actualiser les règles de réécriture pour que les permaliens prennent effet
    flush_rewrite_rules();
}

// Hook lors de l'activation de votre plugin
register_activation_hook(__FILE__, 'my_plugin_set_permalinks');


// Activation, désactivation et désinstallation
register_activation_hook(__FILE__, array('Herdly', 'activate'));
register_deactivation_hook(__FILE__, array('Herdly', 'deactivate'));
register_uninstall_hook(__FILE__, array('Herdly', 'uninstall'));

class Herdly {
    // Méthode appelée lors de l'activation du plugin
    public static function activate() {
        global $wpdb;

        // Création des tables
        $table_locations = $wpdb->prefix . 'locations';
        $table_menu = $wpdb->prefix . 'the_menu';
        $charset_collate = $wpdb->get_charset_collate();

        if ($wpdb->get_var("SHOW TABLES LIKE '$table_locations'") != $table_locations) {
            $sql = "CREATE TABLE $table_locations (
                id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                adress TEXT NOT NULL,
                hours TEXT NOT NULL,
                PRIMARY KEY (id)
            ) $charset_collate;";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        if ($wpdb->get_var("SHOW TABLES LIKE '$table_menu'") != $table_menu) {
            $sql = "CREATE TABLE $table_menu (
                id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                location_id BIGINT(20) UNSIGNED NOT NULL,
                menu_name VARCHAR(255) NOT NULL,
                menu_items TEXT NOT NULL,
                PRIMARY KEY (id),
                FOREIGN KEY (location_id) REFERENCES $table_locations(id)
            ) $charset_collate;";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

    // Méthodes pour désactiver, désinstaller, et mettre à jour le plugin
    public static function deactivate() { }
    public static function uninstall() { }
    public static function upgrade($upgrader, $options) { }
}

function enqueue_mobile_links_bar_script() {
    // Enregistrez le script JavaScript pour le chargement dans le footer
    wp_enqueue_script(
        'change-svg-stroke', // Identifiant du script
        plugin_dir_url(__FILE__) . 'admin/js/mlb-settings.js', // Chemin vers le fichier JS
        array(), // Aucune dépendance
        null, // Pas de numéro de version (ou spécifiez-en un)
        true // Charger le script dans le footer
    );
}
// Utilisez wp_enqueue_scripts pour enregistrer vos scripts côté frontend
add_action('wp_enqueue_scripts', 'enqueue_mobile_links_bar_script');
