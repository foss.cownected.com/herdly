<?php
/**
 * REST API: WP_REST_Posts_Controller class
 *
 * @package WordPress
 * @subpackage REST_API
 * @since 4.7.0
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Empêche l'accès direct
}
/**
 * security parameters for cookies
*/

//add_filter('send_auth_cookies', function ($cookies) {
//    if(!is_array($cookies)) return $cookies;
//    foreach ($cookies as &$cookie) {
//        if (strpos($cookie, 'wordpress_logged_in') !== false
//            || strpos($cookie, 'wordpress_sec') !== false) {
//            (WP_DEBUG) ? $cookie .= '; SameSite=Lax;' : $cookie .= '; SameSite=None; Secure';
//        }
//    }
//    return $cookies;
//});

// Inclure les fonctions utilisées par les routes REST
require_once plugin_dir_path( __FILE__ ) . 'api-functions.php';

/**
 * Webhook
 *
 * Core class to access posts via the REST API.
 *
 * @since 4.7.0
 *
 * @see herdlyAPI
 */
new herdlyAPI();

class herdlyAPI {
    private $namespace = 'herdly';
    private $version = 'v1';

    public function __construct() {
        add_action('rest_api_init', array($this, 'register_routes'));
    }

    public function register_routes() {
        register_rest_route(
            $this->namespace,
            '/' . $this->version . '/location/(?P<id>[\d]+)',
            [
                'methods' => WP_REST_Server::READABLE,
                'callback' => [$this, 'get_opening_hours'],
                'permission_callback' => '__return_true',
                'schema' => [$this, 'get_public_item_schema'],
            ]
        );

        register_rest_route(
            $this->namespace,
            '/' . $this->version . '/mlb_icons_color',
            [
                'methods' => WP_REST_Server::READABLE,
                'callback' => [$this, 'get_mlb_icons_color'],
                'permission_callback' => '__return_true',
                'schema' => [$this, 'get_public_item_schema'],
            ]
        );
    }


    public function get_mlb_icons_color() {

        $mlb_icons_color = get_option('mlb_icons_color');

        // Log des métadonnées pour déboguer
        error_log('Métadonnées récupérées pour la couleur des icônes de la mobile barre ' . print_r($mlb_icons_color, true));


        return new WP_REST_Response($mlb_icons_color, 200);
    }

    public function get_opening_hours($request) {
        $id = intval($request->get_param('id'));

        // Récupérer les métadonnées pour l'ID spécifié
        $opening_times = get_post_meta($id, '_opening_times', true);

        // Log des métadonnées pour déboguer
        error_log('Métadonnées récupérées pour l\'ID ' . $id . ': ' . print_r($opening_times, true));

        // Par défaut, tous les jours sont fermés
        $opening_hours = [
            'monday'    => [],
            'tuesday'   => [],
            'wednesday' => [],
            'thursday'  => [],
            'friday'    => [],
            'saturday'  => [],
            'sunday'    => []
        ];

        if (!empty($opening_times) && is_array($opening_times)) {
            foreach ($opening_times as $day => $times) {
                if (is_array($times)) {
                    $formatted_times = [];

                    foreach ($times as $time_group) {
                        if (isset($time_group['start'], $time_group['stop'])) {
                            $formatted_times[] = [
                                'start' => $time_group['start'],
                                'stop'  => $time_group['stop'],
                            ];
                        }
                    }

                    $opening_hours[$day] = $formatted_times;
                }
            }
        }

        return new WP_REST_Response($opening_hours, 200);
    }

}
