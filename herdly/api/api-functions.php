<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Empêche l'accès direct
}

// Fonction pour récupérer les données des emplacements
function herdly_get_locations( WP_REST_Request $request ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'locations';

    $results = $wpdb->get_results( "SELECT * FROM $table_name", ARRAY_A );

    if ( empty( $results ) ) {
        return new WP_REST_Response( array(
            'message' => 'No locations found.',
        ), 404 );
    }

    return new WP_REST_Response( $results, 200 );
}

// Fonction pour sauvegarder un emplacement
function herdly_save_location( WP_REST_Request $request ) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'locations';

    $params = $request->get_json_params();
    $name   = sanitize_text_field( $params['name'] ?? '' );
    $address = sanitize_textarea_field( $params['adress'] ?? '' );
    $hours   = sanitize_textarea_field( $params['hours'] ?? '' );

    if ( empty( $name ) || empty( $address ) || empty( $hours ) ) {
        return new WP_REST_Response( array(
            'error' => 'Missing required fields: name, address, or hours.',
        ), 400 );
    }

    $result = $wpdb->insert( $table_name, array(
        'name'   => $name,
        'adress' => $address,
        'hours'  => $hours,
    ));

    if ( $result === false ) {
        return new WP_REST_Response( array(
            'error' => 'Failed to insert data.',
        ), 500 );
    }

    return new WP_REST_Response( array(
        'message' => 'Location saved successfully!',
        'id'      => $wpdb->insert_id,
    ), 200 );
}

// Fonction de vérification des permissions
function herdly_check_permissions() {
    return current_user_can( 'edit_posts' );
}
