<?php
// Ajout du menu principal Herdly avec icône SVG
function herdly_add_admin_menus() {
    add_menu_page(
        'Herdly',                    // Titre de la page
        'Herdly',                    // Texte du menu
        'manage_options',            // Capacité requise
        'herdly',                    // Slug de la page
        'herdly_render_main_page',   // Fonction de rappel pour afficher la page principale
        '',                          // Icône du menu (vide car on utilise SVG)
        26                           // Position du menu
    );
    // Sous-menu General Info.
    add_submenu_page(
        'herdly',                    // Slug du menu parent
        'General Information',                  // Titre de la page
        'General Info.',                  // Texte du sous-menu
        'manage_options',            // Capacité requise
        'herdly_general_info',           // Slug de la page
        'herdly_general_info_settings_page'// Fonction de rappel pour afficher la page de réglages
    );
    // Sous-menu Mobile Links Bar
    add_submenu_page(
        'herdly',                    // Slug du menu parent
        'Mobile Links Bar Settings',                  // Titre de la page
        'Mobile Links Bar',                  // Texte du sous-menu
        'manage_options',            // Capacité requise
        'mobile-links-bar',           // Slug de la page
        'mobile_links_bar_settings_page'// Fonction de rappel pour afficher la page de réglages
    );
    // Sous-menu Settings
    add_submenu_page(
        'herdly',                    // Slug du menu parent
        'Settings',                  // Titre de la page
        'Settings',                  // Texte du sous-menu
        'manage_options',            // Capacité requise
        'herdly-settings',           // Slug de la page
        'herdly_render_settings_page'// Fonction de rappel pour afficher la page de réglages
    );

}
add_action('admin_menu', 'herdly_add_admin_menus');




// PAGE PRINCIPALE
function herdly_render_main_page() {
    ?>
    <div class="wrap">
        <h1>Welcome to Herdly</h1>
        <p>Bienvenue sur la page principale de Herdly.</p>
    </div>
    <?php
}




// PAGE SETTINGS
// Fonction pour afficher la page de réglages
function herdly_render_settings_page() {
    ?>
    <div class="wrap">
        <h1>Herdly Settings</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields('herdly_settings_group');
            do_settings_sections('herdly-settings');
            submit_button();
            ?>
        </form>
    </div>
    <?php
}
// Enregistrement des réglages
function herdly_register_settings() {
    add_option('herdly_is_restaurant', 0);
    add_option('herdly_menu_shared', 0);
    register_setting('herdly_settings_group', 'herdly_is_restaurant');
    register_setting('herdly_settings_group', 'herdly_menu_shared');


    add_settings_section(
        'herdly_settings_section',       // ID
        'Restaurant Settings',           // Titre
        null,                            // Callback
        'herdly-settings'                // Page
    );



    add_settings_field(
        'herdly_is_restaurant',          // ID
        'This is a restaurant',          // Label
        'herdly_render_restaurant_checkbox', // Callback
        'herdly-settings',               // Page
        'herdly_settings_section'        // Section
    );

    add_settings_field(
        'herdly_menu_shared',            // ID
        'Menu is shared across all locations', // Label
        'herdly_render_menu_shared_checkbox', // Callback
        'herdly-settings',               // Page
        'herdly_settings_section'        // Section
    );
}
add_action('admin_init', 'herdly_register_settings');



// Rendu de la case à cocher
function herdly_render_restaurant_checkbox() {
    $value = get_option('herdly_is_restaurant');
    ?>
    <input type="checkbox" name="herdly_is_restaurant" value="1" <?php checked(1, $value, true); ?> />
    <?php
}
// SI C'est un restaurant, est ce que le menu est commun à toutes les locations?
function herdly_render_menu_shared_checkbox() {
    $is_restaurant = get_option('herdly_is_restaurant');
    $value = get_option('herdly_menu_shared');

    // Afficher la case seulement si "This is a restaurant" est activé
    if ($is_restaurant) {
        ?>
        <input type="checkbox" name="herdly_menu_shared" value="1" <?php checked(1, $value, true); ?> />
        <?php
    } else {
        echo '<p>' . __('This option is only available for restaurants.', 'textdomain') . '</p>';
    }
}

// Fonction pour afficher la page General Information avec du CSS
function herdly_general_info_settings_page() {
    ?>
    <div class="wrap">
        <h1>General Information</h1>
        <p>Provide the main information about your establishment. Leave fields blank if you don't want them to appear.</p>

        <style>
            .herdly-settings-section {
                margin-bottom: 30px;
            }
            .herdly-settings-section h2 {
                border-bottom: 2px solid #fff;
                padding-bottom: 5px;
                margin-bottom: 15px;
            }
            .herdly-settings-section table {
                width: 100%;
            }
            .herdly-settings-section th {
                text-align: left;
                width: 20%;
            }
            .herdly-settings-section td {
                width: 80%;
            }
            .herdly-street-adress, .herdly-code-city, .herdly-country {
                display: flex;
                flex-wrap: wrap;
                gap: 5px;
            }
            .herdly-country{
                margin-bottom: 15px;
                align-items: center;
            }

        </style>

        <form method="post" action="options.php">
            <?php
            settings_fields('herdly_general_info_options_group');
            do_settings_sections('herdly_general_info');
            ?>

            <!-- Location Information Section -->
            <div class="herdly-settings-section">
                <h2>Location Information</h2>
                <table>
                    <tr valign="top">
                        <th scope="row">Street Address & Number</th>
                        <td>
                            <div class="herdly-street-adress">
                                <input type="text" id="gi_street_address" name="gi_street_address" value="<?php echo esc_attr(get_option('gi_street_address')); ?>" placeholder="Street Address" />
                                <input style="width: 80px" type="number" id="gi_address_number" name="gi_address_number" value="<?php echo esc_attr(get_option('gi_address_number')); ?>" placeholder="Number" />
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Country</th>
                        <td>
                            <div class="herdly-code-city">
                                <input type="text" id="gi_postal_code" name="gi_postal_code" value="<?php echo esc_attr(get_option('gi_postal_code')); ?>" placeholder="Postal Code" />
                                <input type="text" id="gi_city" name="gi_city" value="<?php echo esc_attr(get_option('gi_city')); ?>" placeholder="City" />
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Country</th>
                        <td>
                            <div class="herdly-country">
                                <input type="text" id="gi_country" name="gi_country" value="<?php echo esc_attr(get_option('gi_country')); ?>" placeholder="Country" />
                                <div class="display-country">
                                <input type="checkbox" name="gi_display_country" value="1" <?php checked(1, get_option('gi_display_country'), true); ?> />
                                <span>Display Country</span>
                                </div>
                            </div>
                        </td>

                    </tr>

                    <tr valign="top">
                        <th scope="row">Google Maps URL</th>
                        <td><input type="text" id="gi_google_maps" name="gi_google_maps" value="<?php echo esc_attr(get_option('gi_google_maps')); ?>" placeholder="Google Maps URL" /></td>
                    </tr>
                </table>
            </div>

            <!-- Contact Information Section -->
            <div class="herdly-settings-section">
                <h2>Contact Information</h2>
                <table>
                    <tr valign="top">
                        <th scope="row">Email & Telephone</th>
                        <td>
                            <div class="herdly-inline-fields">
                                <input type="email" id="gi_mail_address" name="gi_mail_address" value="<?php echo esc_attr(get_option('gi_mail_address')); ?>" placeholder="Email Address" />
                                <input type="tel" id="gi_phone_number" name="gi_phone_number" value="<?php echo esc_attr(get_option('gi_phone_number')); ?>" placeholder="Phone Number" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- Social Media Links Section -->
            <div class="herdly-settings-section">
                <h2>Social Media</h2>
                <table>
                    <tr valign="top">
                        <th scope="row">Facebook URL</th>
                        <td><input type="url" id="gi_facebook" name="gi_facebook" value="<?php echo esc_attr(get_option('gi_facebook')); ?>" placeholder="Facebook URL" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Twitter URL</th>
                        <td><input type="url" id="gi_twitter" name="gi_twitter" value="<?php echo esc_attr(get_option('gi_twitter')); ?>" placeholder="Twitter URL" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Instagram URL</th>
                        <td><input type="url" id="gi_instagram" name="gi_instagram" value="<?php echo esc_attr(get_option('gi_instagram')); ?>" placeholder="Instagram URL" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">TikTok URL</th>
                        <td><input type="url" id="gi_tiktok" name="gi_tiktok" value="<?php echo esc_attr(get_option('gi_tiktok')); ?>" placeholder="TikTok URL" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">LinkedIn URL</th>
                        <td><input type="url" id="gi_linkedin" name="gi_linkedin" value="<?php echo esc_attr(get_option('gi_linkedin')); ?>" placeholder="LinkedIn URL" /></td>
                    </tr>
                </table>
            </div>

            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}

// Fonction pour enregistrer les paramètres dans la base de données
function register_herdly_general_info_settings() {
    register_setting('herdly_general_info_options_group', 'gi_street_address');
    register_setting('herdly_general_info_options_group', 'gi_address_number');
    register_setting('herdly_general_info_options_group', 'gi_postal_code');
    register_setting('herdly_general_info_options_group', 'gi_city');
    register_setting('herdly_general_info_options_group', 'gi_country');
    register_setting('herdly_general_info_options_group', 'gi_google_maps');
    register_setting('herdly_general_info_options_group', 'gi_mail_address');
    register_setting('herdly_general_info_options_group', 'gi_phone_number');
    register_setting('herdly_general_info_options_group', 'gi_facebook');
    register_setting('herdly_general_info_options_group', 'gi_twitter');
    register_setting('herdly_general_info_options_group', 'gi_instagram');
    register_setting('herdly_general_info_options_group', 'gi_tiktok');
    register_setting('herdly_general_info_options_group', 'gi_linkedin');

    register_setting('herdly_general_info_options_group', 'mlb_display_on_mobile');
    register_setting('herdly_general_info_options_group', 'mlb_icons_style');

}
add_action('admin_init', 'register_herdly_general_info_settings');



// Fonction pour récupérer et concaténer l'adresse complète
function get_full_address() {
    $street_address = get_option('gi_street_address');
    $address_number = get_option('gi_address_number');
    $postal_code = get_option('gi_postal_code');
    $city = get_option('gi_city');
    $country = get_option('gi_country');
    $display_country = get_option('gi_display_country'); // Correction ici

    // Concaténation de l'adresse complète
    $full_address = $street_address . ' ' . $address_number . ', ' . $postal_code . ' ' . $city;

    // Ajout du pays si l'option est activée
    if ($display_country) {
        $full_address .= ' ' . $country;
    }

    // Suppression des espaces en trop si une des valeurs est vide
    $full_address = trim(preg_replace('/\s+/', ' ', $full_address));

    return $full_address;
}

// Exemple d'utilisation de la fonction get_full_address
//$complete_address = get_full_address();
//echo 'Full Address: ' . $complete_address;




function mobile_links_bar_settings_page() {
    ?>
    <div class="wrap">
        <h1>Mobile Links Bar Settings</h1>
        <p>Here you can configure the Mobile Bar Link</p>
        <form method="post" action="options.php">
            <?php
            settings_fields('herdly_mlb_options_group');
            do_settings_sections('mobile-links-bar');
            ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Display on All Pages</th>
                    <td>
                        <input type="checkbox" name="mlb_display_on_all_pages" value="1" <?php checked(1, get_option('mlb_display_on_all_pages')); ?> />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Display Only on Mobile</th>
                    <td>
                        <input type="checkbox" name="mlb_display_on_mobile" value="1" <?php checked(1, get_option('mlb_display_on_mobile')); ?> />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Select Icon Style</th>
                    <td>
                        <select name="mlb_icons_style">
                            <option value="chunk" <?php selected(get_option('mlb_icons_style'), 'chunk'); ?>>Chunk</option>
                            <option value="classic" <?php selected(get_option('mlb_icons_style'), 'classic'); ?>>Classic</option>
                            <option value="drawing" <?php selected(get_option('mlb_icons_style'), 'drawing'); ?>>Drawing</option>
                            <option value="funky" <?php selected(get_option('mlb_icons_style'), 'funky'); ?>>Funky</option>
                            <option value="line" <?php selected(get_option('mlb_icons_style'), 'line'); ?>>Line</option>
                            <option value="outline" <?php selected(get_option('mlb_icons_style'), 'outline'); ?>>Outline</option>
                            <option value="pixels" <?php selected(get_option('mlb_icons_style'), 'pixels'); ?>>Pixels</option>
                            <option value="round" <?php selected(get_option('mlb_icons_style'), 'round'); ?>>Round</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                <tr valign="top">
                    <th scope="row">Select Icons Size</th>
                    <td>
                        <input type="range" id="mlb_icons_size_range" name="mlb_icons_size" min="0" max="50" value="<?php echo esc_attr(get_option('mlb_icons_size', 10)); ?>" />
                        <span id="mlb_icons_size_value"><?php echo esc_attr(get_option('mlb_icons_size', 25)); ?> px</span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Select Icons Color</th>
                    <td>
                        <input type="color" name="mlb_icons_color" value="<?php echo esc_attr(get_option('mlb_icons_color')); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Padding Top-Bottom</th>
                    <td>
                        <input type="range" id="mlb_padding_top_bottom_range" name="mlb_padding_top_bottom" min="0" max="50" value="<?php echo esc_attr(get_option('mlb_padding_top_bottom', 10)); ?>" />
                        <span id="mlb_padding_top_bottom_value"><?php echo esc_attr(get_option('mlb_padding_top_bottom', 10)); ?> px</span>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Padding Left-Right</th>
                    <td>
                        <input type="range" id="mlb_padding_left_right_range" name="mlb_padding_left_right" min="0" max="50" value="<?php echo esc_attr(get_option('mlb_padding_left_right', 10)); ?>" />
                        <span id="mlb_padding_left_right_value"><?php echo esc_attr(get_option('mlb_padding_left_right', 10)); ?> px</span>
                    </td>
                </tr>
                <!-- Checkbox for No Background Color -->
                <tr valign="top">
                    <th scope="row">No Background Color</th>
                    <td style="display: flex; align-items: center; gap:5px;">
                        <input type="checkbox" name="mlb_no_background" value="1" <?php checked(1, get_option('mlb_no_background')); ?> />
                        <p class="description">*Check this box to make the background transparent and hide the background color input.</p>
                    </td>
                </tr>

                <tr valign="top" class="background-color-row" style="<?php echo get_option('mlb_no_background') ? 'display:none;' : ''; ?>">
                    <th scope="row">Background Color</th>
                    <td>
                        <input type="color" id="mlb_background_color" name="mlb_background" value="<?php echo esc_attr(get_option('mlb_background', '#ffffff')); ?>" />
                    </td>
                </tr>
                <tr valign="top" class="background-color-row" style="<?php echo get_option('mlb_no_background') ? 'display:none;' : ''; ?>">
                    <th scope="row">Background Opacity</th>
                    <td>
                        <input type="range" id="mlb_background_opacity" name="mlb_background_opacity" min="0" max="100" value="<?php echo esc_attr(get_option('mlb_background_opacity', 100)); ?>" />
                        <span id="mlb_background_opacity_value"><?php echo esc_attr(get_option('mlb_background_opacity', 100)); ?>%</span>
                    </td>
                </tr>
                <tr valign="top" class="background-color-row" style="<?php echo get_option('mlb_no_background') ? 'display:none;' : ''; ?>">
                    <th scope="row">Background Blur Effect</th>
                    <td style="display: flex; align-items: center; gap:5px;">
                    <input type="checkbox" name="mlb_background_blur" value="1" <?php checked(1, get_option('mlb_background_blur')); ?> />
                        <p class="description">*Check this box if you want the background to have a blur effect.</p>
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>

        <!-- Aperçu de la Mobile Links Bar -->
        <h2>Preview</h2>
        <div class="mobile-links-bar-preview" style="border: 1px solid #ccc; margin-top: 20px;">
            <?php
            echo render_block([
                'blockName' => 'herdly/mobile-links-bar',
            ]);
            ?>
        </div>
    </div>
    <style>
        #wpfooter{
            position: relative;
        }
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Sélectionner tous les sliders et leurs éléments de valeur associés
            const sliders = [
                { slider: document.getElementById('mlb_padding_top_bottom_range'), valueDisplay: document.getElementById('mlb_padding_top_bottom_value') },
                { slider: document.getElementById('mlb_padding_left_right_range'), valueDisplay: document.getElementById('mlb_padding_left_right_value') },
                { slider: document.getElementById('mlb_icons_size_range'), valueDisplay: document.getElementById('mlb_icons_size_value') },
                { slider: document.getElementById('mlb_background_opacity'), valueDisplay: document.getElementById('mlb_background_opacity_value') },
            ];

            // Boucle à travers tous les sliders et met à jour la valeur affichée en temps réel
            sliders.forEach(({ slider, valueDisplay }) => {
                if (slider && valueDisplay) {  // Vérifie que l'élément existe
                    // Initialiser la valeur affichée
                    valueDisplay.textContent = slider.value + 'px';

                    // Ajouter un événement d'entrée pour mettre à jour la valeur affichée
                    slider.addEventListener('input', function() {
                        valueDisplay.textContent = slider.value + 'px';
                    });
                }
            });
        });
    </script>
    <?php
}


// A REMPLACER



function register_herdly_mlb_settings() {
    register_setting('herdly_mlb_options_group', 'mlb_display_on_all_pages');
    register_setting('herdly_mlb_options_group', 'mlb_display_on_mobile');
    register_setting('herdly_mlb_options_group', 'mlb_icons_style');
    register_setting('herdly_mlb_options_group', 'mlb_icons_size');
    register_setting('herdly_mlb_options_group', 'mlb_icons_color');
    register_setting('herdly_mlb_options_group', 'mlb_padding_top_bottom');
    register_setting('herdly_mlb_options_group', 'mlb_padding_left_right');
    register_setting('herdly_mlb_options_group', 'mlb_background');
    register_setting('herdly_mlb_options_group', 'mlb_background_opacity');
    register_setting('herdly_mlb_options_group', 'mlb_background_blur');
    register_setting('herdly_mlb_options_group', 'mlb_no_background');
}
add_action('admin_init', 'register_herdly_mlb_settings');

// Ajouter les styles dynamiques dans le front-end
function add_mobile_links_bar_dynamic_styles() {
    $padding_top_bottom = get_option('mlb_padding_top_bottom', 0); // Valeur par défaut : 0
    $padding_left_right = get_option('mlb_padding_left_right', 0); // Valeur par défaut : 0
    $widthHeight = get_option('mlb_icons_size', 35);

    $isTransparent = get_option('mlb_no_background');
    $background_color = $isTransparent ? 'transparent' : get_option('mlb_background', '#ffffff'); // Par défaut : blanc
    $background_opacity = get_option('mlb_background_opacity', 1); // Valeur par défaut : 1 (opacité maximale)
    $background_blur = get_option('mlb_background_blur');

    if (!$isTransparent && $background_color !== 'transparent') {
        $background_color = hex_to_rgba($background_color, $background_opacity);
    }

    echo '<!-- Mobile Links Bar Styles -->';
    echo '<style>';
    echo '.mobile-links-bar {';
    echo 'height: ' . esc_attr($widthHeight) . 'px !important;';
    echo 'padding: ' . esc_attr($padding_top_bottom) . 'px ' . esc_attr($padding_left_right) . 'px !important;';
    echo 'background-color: ' . esc_attr($background_color) . ';';
    if($background_blur){
        echo 'backdrop-filter: blur(10px); -webkit-backdrop-filter: blur(10px);';
    }
    echo '}';
    echo '.mobile-links-bar>div>a, .mobile-links-bar>div>div:not(.social-links-nav), .mobile-links-bar object {';
    echo 'width: ' . esc_attr($widthHeight) . 'px !important; height: ' . esc_attr($widthHeight) . 'px !important; display:block !important;';
    echo '}';
    echo '.social-links-nav {';
    echo 'background-color: ' . esc_attr($background_color) . ';';
    if($background_blur){
        echo 'backdrop-filter: blur(10px) !important; -webkit-backdrop-filter: blur(10px);';
    }
    echo '}';

    echo '</style>';
}
add_action('wp_head', 'add_mobile_links_bar_dynamic_styles');

function add_mobile_links_bar_admin_styles() {
    $padding_top_bottom = get_option('mlb_padding_top_bottom', 0); // Valeur par défaut : 0
    $padding_left_right = get_option('mlb_padding_left_right', 0); // Valeur par défaut : 0
    $widthHeight = get_option('mlb_icons_size', 35);

    $isTransparent = get_option('mlb_no_background');
    $background_color = $isTransparent ? 'transparent' : get_option('mlb_background', '#ffffff'); // Par défaut : blanc
    $background_opacity = get_option('mlb_background_opacity', 1); // Valeur par défaut : 1 (opacité maximale)
    $background_blur = get_option('mlb_background_blur');

    if (!$isTransparent && $background_color !== 'transparent') {
        $background_color = hex_to_rgba($background_color, $background_opacity);
    }

    echo '<!-- Mobile Links Bar Styles -->';
    echo '<style>';
    echo '.mobile-links-bar {';
    echo 'height: ' . esc_attr($widthHeight) . 'px !important;';
    echo 'padding: ' . esc_attr($padding_top_bottom) . 'px ' . esc_attr($padding_left_right) . 'px !important;';
    echo 'background-color: ' . esc_attr($background_color) . ';';
    if($background_blur){
        echo 'backdrop-filter: blur(10px); -webkit-backdrop-filter: blur(10px);';
    }
    echo '}';
    echo '.mobile-links-bar>div>a, .mobile-links-bar>div>div:not(.social-links-nav), .mobile-links-bar object {';
    echo 'width: ' . esc_attr($widthHeight) . 'px !important; height: ' . esc_attr($widthHeight) . 'px !important; display:block; pointer-events: auto;';
    echo '}';

    echo '</style>';
}

// Ajouter la fonction au hook 'admin_head' pour injecter les styles dans l'admin.
add_action('admin_head', 'add_mobile_links_bar_admin_styles');


function hex_to_rgba($hex, $opacity) {
    $hex = str_replace('#', '', $hex);

    if (strlen($hex) === 6) {
        list($r, $g, $b) = [
            hexdec(substr($hex, 0, 2)),
            hexdec(substr($hex, 2, 2)),
            hexdec(substr($hex, 4, 2)),
        ];
    } elseif (strlen($hex) === 3) {
        list($r, $g, $b) = [
            hexdec(str_repeat(substr($hex, 0, 1), 2)),
            hexdec(str_repeat(substr($hex, 1, 1), 2)),
            hexdec(str_repeat(substr($hex, 2, 1), 2)),
        ];
    } else {
        return 'rgb(0 0 0 / 100%)'; // Par défaut : noir
    }


    return "rgb($r $g $b / $opacity%)";
}

function render_mobile_links_bar() {
    $display_on_all_pages = get_option('mlb_display_on_all_pages');
    if ($display_on_all_pages) {
        echo render_block(['blockName' => 'herdly/mobile-links-bar']); // Rendre le block comme vous le souhaitez
    } else {
        echo '<!-- Mobile Links Bar is disabled -->';
    }
}
add_action('wp_footer', 'render_mobile_links_bar');

register_setting(
    'herdly_general_info_options_group',
    'gi_street_address',
    [
        'sanitize_callback' => 'sanitize_text_field'
    ]
);


// Ajout du CSS pour l'icône SVG
function herdly_admin_menu_styles() {
    echo '
    <style>
        #adminmenu .toplevel_page_herdly div.wp-menu-image:before {
            content: "";
            display: inline-block;
            background: url("'. esc_attr(plugin_dir_url(__FILE__)) .'assets/img/herdly.svg") no-repeat center center;
            background-size: contain;
            width: 20px; /* Ajustez la taille si nécessaire */
            height: 20px; /* Ajustez la taille si nécessaire */
        }
    </style>';
}
add_action('admin_head', 'herdly_admin_menu_styles');

