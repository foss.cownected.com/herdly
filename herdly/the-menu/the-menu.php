<?php

if (!defined('ABSPATH')) {
    die();
}

// Register Custom Post Type Menu page
function create_menupage_cpt() {

    $labels = array(
        'name'                  => _x('The menu', 'Post Type General Name', 'herdly'),
        'singular_name'         => _x('Menu page', 'Post Type Singular Name', 'herdly'),
        'menu_name'             => _x('The menu', 'Admin Menu text', 'herdly'),
        'name_admin_bar'        => _x('Menu page', 'Add New on Toolbar', 'herdly'),
        'archives'              => __('Menu page Archives', 'herdly'),
        'attributes'            => __('Menu page Attributes', 'herdly'),
        'parent_item_colon'     => __('Parent Menu page:', 'herdly'),
        'all_items'             => __('All The menu', 'herdly'),
        'add_new_item'          => __('Add New Menu page', 'herdly'),
        'add_new'               => __('Add New', 'herdly'),
        'new_item'              => __('New Menu page', 'herdly'),
        'edit_item'             => __('Edit Menu page', 'herdly'),
        'update_item'           => __('Update Menu page', 'herdly'),
        'view_item'             => __('View Menu page', 'herdly'),
        'view_items'            => __('View The menu', 'herdly'),
        'search_items'          => __('Search Menu page', 'herdly'),
        'not_found'             => __('Not found', 'herdly'),
        'not_found_in_trash'    => __('Not found in Trash', 'herdly'),
        'featured_image'        => __('Featured Image', 'herdly'),
        'set_featured_image'    => __('Set featured image', 'herdly'),
        'remove_featured_image' => __('Remove featured image', 'herdly'),
        'use_featured_image'    => __('Use as featured image', 'herdly'),
        'insert_into_item'      => __('Insert into Menu page', 'herdly'),
        'uploaded_to_this_item' => __('Uploaded to this Menu page', 'herdly'),
        'items_list'            => __('The menu list', 'herdly'),
        'items_list_navigation' => __('The menu list navigation', 'herdly'),
        'filter_items_list'     => __('Filter The menu list', 'herdly'),
    );

    $args = array(
        'label'               => __('Menu page', 'herdly'),
        'description'         => __('', 'herdly'),
        'labels'              => $labels,
        'menu_icon'           => 'dashicons-book-alt',
        'supports'            => array('title', 'editor', 'revisions', 'custom-fields'),
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'hierarchical'        => false,
        'exclude_from_search' => false,
        'show_in_rest'        => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type('menupage', $args);
}

$isRestaurant = get_option('herdly_is_restaurant');
if ($isRestaurant) {
    add_action('init', 'create_menupage_cpt', 0);
}


// Add Locations Meta Box
function add_locations_meta_box() {
    add_meta_box(
        'locations_meta_box',           // ID
        'Select Locations',             // Title
        'display_locations_meta_box',   // Callback
        'menupage',                     // Post type
        'side',                         // Position
        'default'                       // Priority
    );
}
$isSharedMenu = get_option('herdly_menu_shared');
if(!$isSharedMenu){
    add_action('add_meta_boxes', 'add_locations_meta_box');

}

// Display Locations Meta Box
function display_locations_meta_box($post) {
    // Récupérer toutes les locations disponibles
    $locations = get_posts(array(
        'post_type'      => 'location',
        'posts_per_page' => -1,
        'post_status'    => 'publish'
    ));

    // Récupérer les locations déjà associées à ce 'menupage'
    $selected_locations = get_post_meta($post->ID, '_associated_locations', true);
    if (!$selected_locations) {
        $selected_locations = array();
    }

    // Affichage de la case "All Restaurants"
    $all_restaurants_checked = in_array('all', $selected_locations) ? 'checked' : '';
    echo '<label>';
    echo '<input type="checkbox" name="associated_locations[]" value="all" ' . $all_restaurants_checked . '> ' . __('All Restaurants', 'textdomain');
    echo '</label><br>';

    // Affichage des locations disponibles avec des cases à cocher
    echo '<ul>';
    if ($locations) {
        foreach ($locations as $location) {
            $checked = in_array($location->ID, $selected_locations) ? 'checked' : '';
            echo '<li>';
            echo '<label>';
            echo '<input type="checkbox" name="associated_locations[]" value="' . $location->ID . '" ' . $checked . '> ' . esc_html($location->post_title);
            echo '</label>';
            echo '</li>';
        }
    } else {
        echo '<li>No locations found.</li>';
    }
    echo '</ul>';
}

// Save Locations Meta Box Data
function save_locations_meta_box($post_id) {
    // Éviter les sauvegardes automatiques et vérifier les permissions
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // Vérifier le type de requête et les permissions de l'utilisateur
    if (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    // Si le formulaire ne contient pas `associated_locations`, cela signifie que toutes les cases ont été décochées
    if (!isset($_POST['associated_locations'])) {
        delete_post_meta($post_id, '_associated_locations');
        return $post_id;
    }

    // Récupérer les locations sélectionnées
    $selected_locations = $_POST['associated_locations'];

    // Si "All Restaurants" est sélectionné
    if (in_array('all', $selected_locations)) {
        // Récupérer toutes les locations
        $locations = get_posts(array(
            'post_type'      => 'location',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        ));

        // Extraire les IDs des locations
        $selected_locations = array_map(function($location) {
            return $location->ID;
        }, $locations);
    }

    // Sauvegarder ou supprimer les métadonnées
    if (empty($selected_locations)) {
        delete_post_meta($post_id, '_associated_locations');
    } else {
        update_post_meta($post_id, '_associated_locations', $selected_locations);
    }

    return $post_id;
}

add_action('save_post', 'save_locations_meta_box');
