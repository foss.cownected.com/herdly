document.addEventListener('DOMContentLoaded', function() {
    // Fonction pour ajouter un nouveau groupe de temps
    function addTimeGroup(day) {
        const daySection = document.querySelector(`.day-section[data-day="${day}"]`);
        console.log(daySection); // Pour vérifier que daySection n'est pas null

        if (!daySection) {
            console.error(`La section pour le jour ${day} n'a pas été trouvée.`);
            return; // Ne pas continuer si la section du jour n'existe pas
        }

        // Suite de votre code...
        const timeGroup = document.createElement('div');
        timeGroup.classList.add('time-group');

        const index = daySection.querySelectorAll('.time-group').length;
        timeGroup.setAttribute('data-index', index);

        // Créer les champs input pour start et stop
        const startInput = document.createElement('input');
        startInput.setAttribute('type', 'time');
        startInput.setAttribute('name', `opening_time_${day}_start[]`);
        startInput.required = true;

        const separatorText = document.createTextNode(' > ');

        const stopInput = document.createElement('input');
        stopInput.setAttribute('type', 'time');
        stopInput.setAttribute('name', `opening_time_${day}_stop[]`);
        stopInput.required = true;

        // Créer le bouton de suppression
        const removeButton = document.createElement('button');
        removeButton.type = 'button';
        removeButton.classList.add('remove-time-group');
        removeButton.classList.add('button');
        removeButton.textContent = '✕';
        removeButton.addEventListener('click', function() {
            timeGroup.remove();
        });

        // Ajouter les éléments au groupe de temps
        timeGroup.appendChild(startInput);
        timeGroup.appendChild(separatorText);
        timeGroup.appendChild(stopInput);
        timeGroup.appendChild(removeButton);

        // Ajouter le groupe de temps à la section du jour
        daySection.appendChild(timeGroup);
    }

    // Gérer les événements de clic pour ajouter des groupes de temps
    document.querySelectorAll('.add-time-group').forEach(function(button) {
        button.addEventListener('click', function() {
            const day = button.getAttribute('data-day');
            addTimeGroup(day);
        });
    });

    // Gérer les événements de clic pour supprimer des groupes de temps
    document.querySelectorAll('.remove-time-group').forEach(function(button) {
        button.addEventListener('click', function() {
            const timeGroup = button.closest('.time-group');
            timeGroup.remove();
        });
    });
});
