document.addEventListener('DOMContentLoaded', function() {
    const resetButton = document.querySelector('input[type="reset"]');
    if(resetButton){
        resetButton.addEventListener('click', function() {
            // Réinitialiser chaque champ de texte et de nombre
            const inputs = document.querySelectorAll('#restaurant_location input[type="text"], #restaurant_location input[type="number"]');
            console.log(inputs)
            inputs.forEach(input => {
                input.value = '';
                console.log(input.value);

            });
        });
    };
});