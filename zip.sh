#!/bin/bash
rm herdly.zip
zip -r herdly.zip herdly/* -x '*.git*' -x '*node_modules*' -x 'herdly/blocker/blocks*'